# Hackerfleet Operating System Localization file
# Copyright (C) 2011-2018, Hackerfleet
# This file is distributed under the same license as the HFOS package.
# Heiko 'riot' Weinen <riot@c-base.org>, 2017.
# riot <riot@c-base.org>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: Isomer 1.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-02 18:44+0200\n"
"PO-Revision-Date: 2018-07-08 16:22+0200\n"
"Last-Translator: riot <riot@c-base.org>\n"
"Language-Team: Deutsches Übersetzungsteam\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.11\n"
"Generated-By: Babel 2.4.0\n"

#: ../../source/index.rst:3
msgid "HFOS Documentation"
msgstr "HFOS Dokumentation"

#: ../../source/index.rst
msgid "Version"
msgstr ""

#: ../../source/index.rst:5
msgid "|version|"
msgstr ""

#: ../../source/index.rst
msgid "Release"
msgstr ""

#: ../../source/index.rst:6
msgid "|release|"
msgstr ""

#: ../../source/index.rst
msgid "Date"
msgstr "Datum"

#: ../../source/index.rst:7
msgid "|today|"
msgstr ""

#: ../../source/index.rst:13
msgid "About"
msgstr "Über"

#: ../../source/index.rst:22
msgid "Sailor's Manual"
msgstr "Segler Handbuch"

#: ../../source/index.rst:32
msgid "Developer Documentation"
msgstr "Entwickler Dokum"

#: ../../source/index.rst:60
msgid "Indices and tables"
msgstr "Indizes und Tabellen"

#: ../../source/index.rst:62
msgid ":ref:`Index <genindex>`"
msgstr ""

#: ../../source/index.rst:63
msgid ":ref:`modindex`"
msgstr ""

#: ../../source/index.rst:64
msgid ":ref:`search`"
msgstr ""

#: ../../source/index.rst:65
msgid ":doc:`glossary`"
msgstr ""

#: ../../source/index.rst:66
msgid ":doc:`changes`"
msgstr ""

#: ../../source/index.rst:70
msgid ":doc:`todo`"
msgstr ""

#: ../../source/index.rst:71
msgid ":doc:`readme`"
msgstr ""
